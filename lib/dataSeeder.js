'use strict';

var DockerCommand = require('../models/dockerCommand');

var dataInitializer = function () {

    var initializeData = function(callback) {
           
            var runDockerCommand = new DockerCommand({
                command             : 'run',
                description         : 'Runs a Docker container',
                examples            : [
                    {
                        example: 'docker run imageName',
                        description: 'Creates a running container from the image. Pulls it from Docker Hub if the image is not local'
                    },
                    {
                        example: 'docker run -d -p 8080:3000 imageName',
                        description: 'Runs a container in "daemon" mode with an external port of 8080 and a container port of 3000.'
                    }
                ]});
             
             runDockerCommand.save(function(err) {
                 if (err) {
                     return callback(err);                     
                 }
             });
             
            var psDockerCommand = new DockerCommand({
                command             : 'ps',
                description         : 'Lists containers',
                examples            : [
                    {
                        example: 'docker ps',
                        description: 'Lists all running containers'
                    },
                    {
                        example: 'docker ps -a',
                        description: 'Lists all containers (even if they are not running)'
                    }
                ]});
             
             psDockerCommand.save(function(err) {
                 if (err) {
                     return callback(err);                     
                 }
             });

             var composeDockerCommand = new DockerCommand({
                command             : 'docker-compose',
                description         : 'Defining and running multi-container Docker applications',
                examples            : [
                    {
                        example: 'docker-compose up',
                        description: 'Builds, (re)creates, starts, and attaches to containers for a service.'
                    },
                    {
                        example: 'docker-compose down',
                        description: 'Stops containers and removes containers, networks, volumes, and images created by up'
                    }
                ]});
             
             composeDockerCommand.save(function(err) {
                 if (err) {
                     return callback(err);                     
                 }
             });

        };


    return {
        initializeData: initializeData
    };

}();

module.exports = dataInitializer;