# Node.js with MongoDB and Docker Automation Workflow Demo

Application demo designed to show how Node.js and MongoDB can be run in Docker containers. 
The app uses Mongoose to create a simple database that stores Docker commands and examples.

##To run the app with Docker Containers:

1. Install Docker (http://docker.com/).

2. Open the terminal navigate to the app's folder.

3. Run the commands listed in `node.dockerfile`.

4. Navigate to http://localhost:8080 in your browser to view the site. 


##To run the app with Node.js and MongoDB (without Docker):

1. Install and start MongoDB

2. Install Node.js (http://nodejs.org).

3. Open `config/config.development.json` and adjust the host name to your MongoDB server name (`localhost` normally works if you're running locally). 

4. Run `npm install`.

5. Run `node dbSeeder.js` to get the sample data loaded into MongoDB. Exit the command prompt.

6. Run `node server.js` to start the server.

7. Navigate to http://localhost:8080 in your browser.




