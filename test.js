var superagent = require("superagent"),
    chai = require("chai"),
    expect = chai.expect,
    should = require("should"),
    fs = require('fs');

describe("Tests", function () {
  it("Check docker-compose.yml file exists!", function () {
    expect(fs.existsSync('docker-compose.yml')).to.be.true
  });

  it("Check dockerfile file exists!", function () {
    expect(fs.existsSync('node.dockerfile')).to.be.true
  });

  it("Check shippable.yml file exists!", function () {
    expect(fs.existsSync('shippable.yml')).to.be.true
  });
});


/*describe("Persistence", function () {
  it("should create a thing", function (done) {
    superagent.get("http://localhost:8080/dockertask")
      .end(function (e, res) {
        (e === null).should.equal(true);
        var response = res.body;
        expect(response.created).to.equal(true);
        done();
      });
  });
  it("should retrieve a thing", function (done) {
    superagent.get("http://localhost:8080/dockertask")
      .end(function (e, res) {
        (e === null).should.equal(true);
        var response = res.body;
        expect(response.created).to.equal(false);
        response = response.returnObj;
        response.should.have.property("name", "dockertask");
        done();
      });
  });
});
*/
